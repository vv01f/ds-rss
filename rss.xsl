<xsl:stylesheet encoding="UTF-8" version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!-- xmlns:exsltstr="http://exslt.org/strings" -->
<!-- xmlns:date="http://exslt.org/dates-and-times" extension-element-prefixes="date" -->
<xsl:output omit-xml-declaration="yes"/>
<!--xsl:output method="xml" indent="yes" encoding="utf-8"/-->
<!--xmlns:dc="http://purl.org/dc/elements/1.1/"-->

	<xsl:template match="/schleudern">
<!--DOCTYPE rss-->
<xsl:text disable-output-escaping="yes">&lt;?xml version="1.0" encoding="UTF-8"?&gt;</xsl:text><xsl:text>&#10;</xsl:text>
<xsl:text disable-output-escaping="yes">&lt;?xml-stylesheet type="text/css" href="https://ds.ccc.de/style-rss.css"?&gt;</xsl:text><xsl:text>&#10;</xsl:text>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"><xsl:text>&#10;</xsl:text>
<xsl:text>&#10;</xsl:text>
<channel><xsl:text>&#10;&#9;</xsl:text>
	<xsl:variable name="limitenclosures">0</xsl:variable><!-- 0:no limit, 1:limit is one  -->
	<xsl:variable name="protocol">https://</xsl:variable>
	<xsl:variable name="baseurl">ds.ccc.de</xsl:variable>
	<xsl:variable name="feedfile">ds-feed.xml</xsl:variable>
	<title>Datenschleuder</title>
	<xsl:text>&#10;&#9;</xsl:text>
	<xsl:element name="atom:link">
		<xsl:attribute name="rel">self</xsl:attribute><!-- when atom url is rss url -->
		<xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,'/',$feedfile)"/></xsl:attribute>
		<xsl:attribute name="type">application/rss+xml</xsl:attribute>
	</xsl:element>
	<xsl:text>&#10;&#9;</xsl:text>
<!--
<atom:link href="https://ds.ccc.de/ds-feed.xml" rel="self" type="application/rss+xml" />
-->
	<xsl:element name="link"><xsl:value-of select="concat($protocol,$baseurl,'/')"/></xsl:element><!-- <link>https://ds.ccc.de</link> -->
	<xsl:text>&#10;&#9;</xsl:text>
	<description>Das wissenschaftliche Fachblatt für Datenreisende; ein Organ des Chaos Computer Club.</description>
	<xsl:text>&#10;&#9;</xsl:text>
	<lastBuildDate><xsl:value-of select="$current-date"/></lastBuildDate>
	<xsl:text>&#10;&#9;</xsl:text>
	<!-- lastBuildDate>Tue, 30 Mar 2021 10:27:18 +0000</lastBuildDate-->
		<!-- <xsl:value-of select="date:date-time()"/> -->
		<!-- # \-\-stringparam current-date `date +%Y-%m-%d` # possibility for current date-time-group via XSLT <xsl:value-of select="$current-date"/> -->
	<language>de</language>
	<xsl:text>&#10;&#9;</xsl:text>
	<!--copyright></copyright-->
	<managingEditor>ds@ccc.de (Redaktion)</managingEditor>
	<xsl:text>&#10;&#9;</xsl:text>
	<!--webMaster>ds@ccc.de</webMaster-->
	<ttl>65535</ttl>
	<xsl:text>&#10;&#9;</xsl:text>
	<image>
		<xsl:element name="url"><xsl:value-of select="concat($protocol,$baseurl,'/i/ds_headline.gif')"/></xsl:element><!--	<url>https://ds.ccc.de</url> -->
		<title>Datenschleuder</title>
		<xsl:element name="link"><xsl:value-of select="concat($protocol,$baseurl,'/')"/></xsl:element>
	</image>
	<xsl:text>&#10;&#9;</xsl:text>

<!--
<item>
	<title>ds #98</title>
	<link>/pdfs/ds098.pdf</link>
	<description>Ausgabe #98</description>
</item>
<item>
	<title>ds #97</title>
	<link>/pdfs/ds097.pdf</link>
	<description>Ausgabe #97</description>
</item>
-->

	<xsl:for-each select="schleuder">
<!--
<xsl:for-each select="catalog/cd">
-->
		<xsl:element name="item">
		<xsl:text>&#10;&#9;</xsl:text>
			<!-- we do not have most optional elements -->
			<xsl:element name="title"><xsl:value-of select="concat('Datenschleuder #',@id)"/></xsl:element>
		<xsl:text>&#10;&#9;&#9;</xsl:text>

			<xsl:choose>
				<xsl:when test="not(date)"></xsl:when>
				<xsl:otherwise><xsl:element name="pubDate"><xsl:value-of select="date"/></xsl:element><xsl:text>&#10;&#9;&#9;</xsl:text></xsl:otherwise>
			</xsl:choose>

			<xsl:element name="description">
				<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
				<xsl:element name="h3"><xsl:value-of select="concat('Ausgabe #',@id)"/></xsl:element>
				<xsl:element name="div"><p><xsl:value-of select="teaser"/></p>

				<xsl:if test="string-length(image) &gt; 0">
					<xsl:element name="img"><xsl:attribute name="src">
						<xsl:value-of select="concat($protocol,$baseurl,'/',image)"/></xsl:attribute>
					</xsl:element><xsl:text>&#10;&#9;&#9;</xsl:text>
				</xsl:if>

				<xsl:element name="ul">

					<xsl:if test="string-length(preface) &gt; 0">
						<li><xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,'/',preface)"/></xsl:attribute>Geleitwort</xsl:element></li>
						<xsl:text>&#10;&#9;&#9;</xsl:text>
					</xsl:if>
					<xsl:if test="string-length(references) &gt; 0">
						<li><xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,'/',references)"/></xsl:attribute>Referenzen zur Ausgabe</xsl:element></li>
						<xsl:text>&#10;&#9;&#9;</xsl:text>
					</xsl:if>

				<xsl:for-each select="link"><!-- files to link, bit maybe not add as enclosure -->
<!--
					<xsl:if test="((position() &gt; 1) and ($limitenclosures = 1))">
-->
						<xsl:variable name="type">
							<xsl:choose>
								<xsl:when test="substring(., string-length(.) - 2) = 'pdf'">PDF</xsl:when>
								<xsl:when test="substring(., string-length(.) - 2) = 'zip'">ZIP</xsl:when>
								<xsl:when test="substring(., string-length(.) - 3) = 'html'">HTML</xsl:when>
								<xsl:when test="substring(., string-length(.) - 3) = 'epub'">ePUB</xsl:when>
								<xsl:when test="substring(., string-length(.) - 6) = 'torrent'">Torrent</xsl:when>
								<xsl:otherwise>unknown</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:text disable-output-escaping="yes"> </xsl:text>
						<li><xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,'/',.)"/></xsl:attribute><xsl:value-of select="concat($type,'-Datei')"/></xsl:element></li>
<!--
					</xsl:if>
-->
				</xsl:for-each>
				</xsl:element><!-- /ul -->

				</xsl:element><!-- /div -->

				<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
			</xsl:element>
			<xsl:text>&#10;&#9;&#9;</xsl:text>

			<xsl:variable name="id">
				<xsl:value-of select="@id"/>
			</xsl:variable>

			<xsl:for-each select="link">

				<xsl:variable name="position">
					<xsl:value-of select="position()"/>
				</xsl:variable>

				<xsl:variable name="href">
					<xsl:value-of select="concat($protocol,$baseurl,'/',.)"/>
				</xsl:variable>

				<xsl:variable name="filesize">
					<xsl:choose>
						<xsl:when test="string-length(@filesize) &gt; 0">
							<xsl:value-of select="@filesize"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="../filesize"/><!-- todo: if not available lenth="0" -->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<xsl:variable name="type">
					<xsl:choose>
						<xsl:when test="substring($href, string-length($href) - 2) = 'pdf'">pdf</xsl:when>
						<xsl:when test="substring($href, string-length($href) - 2) = 'zip'">zip</xsl:when>
						<xsl:when test="substring($href, string-length($href) - 3) = 'html'">html</xsl:when>
						<xsl:when test="substring($href, string-length($href) - 3) = 'epub'">epub</xsl:when>
						<xsl:when test="substring($href, string-length($href) - 6) = 'torrent'">torrent</xsl:when>
						<xsl:otherwise>unknown</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<xsl:if test="$position = 1">
					<xsl:if test="$type != 'html'">
						<xsl:element name="guid">
							<xsl:attribute name="isPermaLink">true</xsl:attribute>
							<xsl:value-of select="$href"/>
						</xsl:element><xsl:text>&#10;&#9;&#9;</xsl:text>
					</xsl:if>
					<xsl:if test="$type = 'html'">
						<xsl:element name="guid">
							<xsl:attribute name="isPermaLink">true</xsl:attribute>
							<xsl:value-of select="concat('https://www.offiziere.ch/trust-us/ds/',substring($id, 0, 3))"/>
						</xsl:element><xsl:text>&#10;&#9;&#9;</xsl:text>
					</xsl:if>
					<xsl:element name="link">
						<xsl:value-of select="$href"/>
					</xsl:element><xsl:text>&#10;&#9;&#9;</xsl:text>
				</xsl:if>

				<xsl:if test="((($position &gt; 1) and ($limitenclosures = 0)) or ($position = 1))">
					<xsl:choose>
						<xsl:when test="$type = 'pdf'">

							<xsl:element name="enclosure">
								<xsl:attribute name="url"><xsl:value-of select="$href"/></xsl:attribute>
								<xsl:attribute name="length"><xsl:value-of select="$filesize"/></xsl:attribute>
								<xsl:attribute name="type">application/pdf</xsl:attribute>
							</xsl:element><xsl:text>&#10;&#9;&#9;</xsl:text>

						</xsl:when>
						<xsl:when test="$type = 'epub'">

							<xsl:element name="enclosure">
								<xsl:attribute name="url"><xsl:value-of select="$href"/></xsl:attribute>
								<xsl:attribute name="length"><xsl:value-of select="$filesize"/></xsl:attribute>
								<xsl:attribute name="type">application/epub+zip</xsl:attribute>
							</xsl:element><xsl:text>&#10;&#9;</xsl:text>

						</xsl:when>
<!--
						<xsl:when test="$type = 'zip'">

							<xsl:element name="enclosure">
								<xsl:attribute name="url"><xsl:value-of select="$href"/></xsl:attribute>
								<xsl:attribute name="length"><xsl:value-of select="$filesize"/></xsl:attribute>
								<xsl:attribute name="type">application/zip</xsl:attribute>
							</xsl:element><xsl:text>&#10;&#9;</xsl:text>

						</xsl:when>
-->
						<xsl:when test="$type = 'torrent'">

							<xsl:element name="enclosure">
								<xsl:attribute name="url"><xsl:value-of select="$href"/></xsl:attribute>
								<xsl:attribute name="length"><xsl:value-of select="$filesize"/></xsl:attribute>
								<xsl:attribute name="type">application/octet-stream</xsl:attribute>
							</xsl:element><xsl:text>&#10;&#9;</xsl:text>

						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="enclosure">
								<xsl:attribute name="url"><xsl:value-of select="$href"/></xsl:attribute>
								<xsl:attribute name="length"><xsl:value-of select="$filesize"/></xsl:attribute>
								<xsl:attribute name="type">application/octet-stream</xsl:attribute>
							</xsl:element><xsl:text>&#10;&#9;</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>

			</xsl:for-each>

			<xsl:if test="count(link) = 0">
					<xsl:element name="guid">
						<xsl:attribute name="isPermaLink">true</xsl:attribute>
<!--
						<xsl:value-of select="concat($protocol,$baseurl,'/#',exsltstr:encode-uri(@id, 'true', 'UTF-8'))"/>
-->
						<xsl:value-of select="concat($protocol,$baseurl,'/#',@id)"/>
					</xsl:element><xsl:text>&#10;&#9;</xsl:text>
			</xsl:if>
		</xsl:element><xsl:text>&#10;&#9;</xsl:text>
	</xsl:for-each>

</channel><xsl:text>&#10;&#9;</xsl:text></rss>
	</xsl:template>
</xsl:stylesheet>
