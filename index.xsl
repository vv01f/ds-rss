<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" encoding="UTF-8" version="1.0">
<xsl:output omit-xml-declaration="yes"/>

    <xsl:template match="/schleudern">
		<xsl:variable name="limitelements">0</xsl:variable><!-- limit as integer, 0:no limit  -->
		<xsl:variable name="protocol">https://</xsl:variable>
		<xsl:variable name="baseurl">ds.ccc.de</xsl:variable>

<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
<xsl:element name="html">
	<xsl:attribute name="xml:lang">de</xsl:attribute>
	<xsl:attribute name="lang">de</xsl:attribute>
<xsl:element name="head">
	<meta charset="utf-8"/> 
	<title>Die Datenschleuder</title>
	<meta name="robots" content="index,follow"/>
	<link rel="schema.DC" href="http://purl.org/dc/terms/"/>
	<link rel="alternate" type="application/rss+xml" title="Datenschleuder als RSS-Feed abonnieren" href="ds-feed.xml"/>
	<meta name="dc.publisher" content="Chaos Computer Club" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<xsl:element name="style">
		<xsl:attribute name="type">text/css</xsl:attribute>
		<xsl:attribute name="media">screen,projection</xsl:attribute>
<!--
		<xsl:attribute name="src">style.css</xsl:attribute>
-->
		<xsl:text> @import "style.css"; 
time { font-family: Arial, sans-serif; font-size: 10pt; }</xsl:text>
	</xsl:element>
<!--
	<xsl:element name="style">
		<xsl:attribute name="type">text/css</xsl:attribute>
		<xsl:attribute name="media">all</xsl:attribute>
	</xsl:element>
-->
<!--
	<style type="text/css" media="screen,projection" src="style.css"/>
-->
<!--
	<style type="text/css">
		time { font-family: Arial, sans-serif !important; font-size: 12pt !important; }
	</style>
-->
</xsl:element>
<xsl:element name="body">

	<xsl:element name="p">
		<xsl:attribute name="id">top</xsl:attribute>
		<xsl:element name="img">
			<xsl:attribute name="src">i/ds_headline.gif</xsl:attribute>
			<xsl:attribute name="alt">Die Datenschleuder</xsl:attribute>
		</xsl:element><xsl:element name="br"/>
		<xsl:element name="img">
			<xsl:attribute name="src">i/ds_subline.gif</xsl:attribute>
			<xsl:attribute name="alt">Das wissenschaftliche Fachblatt für Datenreisende – Ein Organ des Chaos Computer Club</xsl:attribute>
		</xsl:element>		
	</xsl:element>
     <p>
       Die Datenschleuder | <a href="contribute.html">Contribute</a> | <a href="download.html">Download</a> | <a href="contact.html">Kontakt</a> | <a href="imprint.html">Impressum/Datenschutz</a> | <a href="order.html">Bestellen</a>
     </p>
     
	<xsl:element name="div">
		<xsl:attribute name="style">background: #dddddd; padding: 1px 15px 10px 15px;</xsl:attribute>
     
		<xsl:element name="h2">
			Wir sind wieder da.
		</xsl:element><xsl:text>&#xa;</xsl:text>

		<xsl:element name="div">
			<xsl:attribute name="style">float:right; margin-left: 10px; margin-right: 20px;</xsl:attribute>

			<span style="display:inline-block;">
				<xsl:element name="a">
					<xsl:attribute name="href"><xsl:value-of select="schleuder[1]/preface"/></xsl:attribute>
					<xsl:element name="img">
						<xsl:attribute name="style">width: 105px;</xsl:attribute>
						<xsl:attribute name="style">height: 150px;</xsl:attribute>
						<xsl:attribute name="src"><xsl:value-of select="schleuder[1]/image"/></xsl:attribute>
						<xsl:attribute name="alt">Die aktuelle Datenschleuder #<xsl:value-of select="schleuder[1]/@id"/></xsl:attribute>
						<xsl:attribute name="title">Letzte Ausgabe vom <xsl:call-template name="GermanDate"><xsl:with-param name="EmailDate" select="schleuder[1]/date" /></xsl:call-template></xsl:attribute>
					</xsl:element>
				</xsl:element><br/>
				<span style="font-size:small;">#<xsl:value-of select="schleuder[1]/@id"/></span>
			</span>
			<xsl:text>&#xa;</xsl:text>
			<span style="display:inline-block;">
				<xsl:element name="a">
					<xsl:attribute name="href"><xsl:value-of select="schleuder[2]/preface"/></xsl:attribute>
					<xsl:element name="img">
						<xsl:attribute name="style">width: 105px;</xsl:attribute>
						<xsl:attribute name="style">height: 150px;</xsl:attribute>
						<xsl:attribute name="src"><xsl:value-of select="schleuder[2]/image"/></xsl:attribute>
						<xsl:attribute name="alt">Die etwas ältere Datenschleuder #<xsl:value-of select="schleuder[2]/@id"/></xsl:attribute>
						<xsl:attribute name="title">Die vorherige Ausgabe vom <xsl:call-template name="GermanDate"><xsl:with-param name="EmailDate" select="schleuder[2]/date" /></xsl:call-template></xsl:attribute>
					</xsl:element>
				</xsl:element><br/>
				<span style="font-size:small;">#<xsl:value-of select="schleuder[2]/@id"/></span>
			</span>
			<xsl:text>&#xa;</xsl:text>
			<span style="display:inline-block;">
				<xsl:element name="a">
					<xsl:attribute name="href"><xsl:value-of select="schleuder[3]/preface"/></xsl:attribute>
					<xsl:element name="img">
						<xsl:attribute name="style">width: 105px;</xsl:attribute>
						<xsl:attribute name="style">height: 150px;</xsl:attribute>
						<xsl:attribute name="src"><xsl:value-of select="schleuder[3]/image"/></xsl:attribute>
						<xsl:attribute name="alt">Eine etwas ältere Datenschleuder #<xsl:value-of select="schleuder[3]/@id"/></xsl:attribute>
						<xsl:attribute name="title">Eine etwas ältere Datenschleuder vom <xsl:call-template name="GermanDate"><xsl:with-param name="EmailDate" select="schleuder[3]/date" /></xsl:call-template>
						</xsl:attribute>
					</xsl:element>
				</xsl:element><br/>
				<span style="font-size:small;">#<xsl:value-of select="schleuder[3]/@id"/></span>
			</span>

		</xsl:element>

		<xsl:text>&#xa;</xsl:text>
		<xsl:for-each select="schleuder">

			<xsl:if test="count(info)&gt;0">
				<xsl:element name="div">
					<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
					<xsl:text>&#xa;</xsl:text>

					<xsl:for-each select="info">
						<p>
							<xsl:element name="time">
								<xsl:attribute name="datetime">
									<xsl:if test="not(@date)"><xsl:value-of select="date"/></xsl:if>
									<xsl:if test="count(@date)&gt;0"><xsl:value-of select="@date"/></xsl:if>
								</xsl:attribute>
								<xsl:call-template name="GermanDate">
									<xsl:with-param name="EmailDate" select="@date" />
								</xsl:call-template>
							</xsl:element><br/><xsl:text>&#xa;</xsl:text>

							<xsl:copy-of select="./node()" />
							<xsl:text>&#xa;</xsl:text>
						</p>
					</xsl:for-each>

				</xsl:element><xsl:text>&#xa;</xsl:text>
			</xsl:if>

			<!-- limit on last n elements -->
			<xsl:if test="(count(teaser)&gt;0) and (($limitelements = 0) or ((position() &lt;= $limitelements) and ($limitelements &gt; 0)))">

				<xsl:element name="p">
					<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
					<xsl:text>&#xa;</xsl:text>

					<xsl:if test="count(date)&gt;0">
					<xsl:element name="time">
						<xsl:attribute name="datetime">
							<xsl:if test="count(date) &gt; 0">
								<xsl:value-of select="date"/>
							</xsl:if>
							<xsl:if test="count(date) = 0">
								<xsl:value-of select="@date"/>
							</xsl:if>
						</xsl:attribute>
						<xsl:if test="count(date) &gt; 0">
							<xsl:call-template name="GermanDate">
								<xsl:with-param name="EmailDate" select="date" />
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="count(date) = 0">
							<xsl:call-template name="GermanDate">
								<xsl:with-param name="EmailDate" select="@date" />
							</xsl:call-template>
						</xsl:if>
					</xsl:element><br/><xsl:text>&#xa;</xsl:text>
					</xsl:if>

					<!-- commented out until better phrasing/positioning found -->
					<!--xsl:if test="count(preface) &gt; 0">
						<xsl:element name="a">
							<xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,preface)"/></xsl:attribute>
							Geleitwort zur <xsl:value-of select="@id"/>. Ausgabe.
						</xsl:element><xsl:text>&#xa;</xsl:text>
					</xsl:if-->
					<!--xsl:if test="count(references) &gt; 0">
						<xsl:element name="a">
							<xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,references)"/></xsl:attribute>
							Rerenzen zur <xsl:value-of select="@id"/>. Ausgabe.
						</xsl:element><xsl:text>&#xa;</xsl:text>
					</xsl:if-->


					<xsl:if test="count(teaser) &gt; 0">
						<xsl:copy-of select="teaser/node()" />
					</xsl:if><br/>
					<xsl:text>&#xa;</xsl:text>
					
				</xsl:element><xsl:text>&#xa;</xsl:text>

			</xsl:if>
		</xsl:for-each>

	<xsl:element name="p">
	 In der <a href="download.html">Download-Sektion</a> gibt es
	 alle Datenschleudern, derer wir in der pdf-Version habhaft
	 werden konnten, zum kostenlosen Herunterladen. Eventuelle Links
	 zu einigen Artikeln sind kurzfristig hinüber, werden jedoch
	 schnellstmöglich wieder hergestellt. Wir bitten um Geduld.
	</xsl:element>

	</xsl:element>
	<xsl:element name="p">
		<xsl:attribute name="style">text-align: right</xsl:attribute>
		<xsl:element name="a">
			<xsl:attribute name="href">#top</xsl:attribute>
			top
		</xsl:element>
	</xsl:element>

</xsl:element><!-- /body -->
</xsl:element><!-- /html -->

    </xsl:template>

	<xsl:template name="copycontent"><!-- match="@*|node()" matches all -->
		<xsl:copy><!-- copy subtree -->
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template name="GermanDate">
    <xsl:param name="EmailDate" />
    <xsl:param name="Date" />
    <xsl:param name="Year" />
    <xsl:param name="Month" />
    <xsl:param name="Day" />

	<xsl:if test="not($EmailDate)"><!-- no date --></xsl:if>
    <xsl:choose>
		<xsl:when test="not($Month)">
			<xsl:variable name="EmailMonth">
				<xsl:value-of select="substring($EmailDate,9,3)"/>
			</xsl:variable>
			<xsl:variable name="GermanMonth">
				<xsl:choose>
					<xsl:when test="$EmailMonth = 'Jan'">Januar</xsl:when>
					<xsl:when test="$EmailMonth = 'Feb'">Februar</xsl:when>
					<xsl:when test="$EmailMonth = 'Mar'">März</xsl:when>
					<xsl:when test="$EmailMonth = 'Apr'">April</xsl:when>
					<xsl:when test="$EmailMonth = 'May'">Mai</xsl:when>
					<xsl:when test="$EmailMonth = 'Jun'">Juni</xsl:when>
						<xsl:when test="$EmailMonth = 'Jul'">Juli</xsl:when>
					<xsl:when test="$EmailMonth = 'Aug'">August</xsl:when>
					<xsl:when test="$EmailMonth = 'Sep'">September</xsl:when>
					<xsl:when test="$EmailMonth = 'Oct'">Oktober</xsl:when>
					<xsl:when test="$EmailMonth = 'Nov'">November</xsl:when>
					<xsl:when test="$EmailMonth = 'Dec'">Dezember</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="GermanDate">
			  <xsl:with-param name="Month" select="$GermanMonth" />
			  <xsl:with-param name="EmailDate" select="$EmailDate" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="not($Day)"><!-- 'Thu, 02 May 2019 23:01:34 +0200' -->
			<xsl:variable name="EmailDay">
				<xsl:value-of select="floor(substring($EmailDate,6,2))"/>
			</xsl:variable>
			<xsl:call-template name="GermanDate">
			  <xsl:with-param name="Month" select="$Month" />
			  <xsl:with-param name="Day" select="$EmailDay" />
			  <xsl:with-param name="EmailDate" select="$EmailDate" />
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="not($Year)">
			<xsl:variable name="EmailYear">
				<xsl:value-of select="substring($EmailDate,13,4)"/>
			</xsl:variable>
			<xsl:call-template name="GermanDate">
			  <xsl:with-param name="Month" select="$Month" />
			  <xsl:with-param name="Day" select="$Day" />
			  <xsl:with-param name="Year" select="$EmailYear" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="concat($Day,'. ',$Month,' ',$Year)"/>
		</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
