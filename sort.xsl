<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" encoding="UTF-8" version="1.0">
<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/schleudern">
<xsl:text disable-output-escaping='yes'>&lt;?xml version="1.0" encoding="UTF-8"?&gt;

&lt;!DOCTYPE schleudern [

&lt;!ELEMENT schleudern (schleuder)&gt;&lt;!-- wrapper --&gt;
&lt;!ELEMENT schleuder (date,image,link,filesize,teaser,preface,references,info)&gt;&lt;!-- issue date (print) --&gt;
&lt;!ATTLIST schleuder id CDATA #REQUIRED&gt;&lt;!-- issue number --&gt;
&lt;!ATTLIST schleuder printonly CDATA #IMPLIED&gt;&lt;!-- marking for not (yet) downloadable issues --&gt;
&lt;!ELEMENT date (#PCDATA)&gt;&lt;!-- date of publication, https://www.w3.org/Protocols/rfc822/ as in cli `date -R` or 'Thu, 02 May 2019 23:01:34 +0200' --&gt;
&lt;!ELEMENT image (#PCDATA)&gt;&lt;!-- usually the cover page --&gt;
&lt;!ELEMENT link (#PCDATA)&gt;&lt;!-- file location or fallback link, here attachments for rss and linked files for download --&gt;
&lt;!ATTLIST link filesize CDATA #IMPLIED&gt;&lt;!-- attribute filesize for multiple attachments: as PDF, ePUB etc. --&gt;
&lt;!ELEMENT filesize (#PCDATA)&gt;&lt;!-- size of a single linked file, needed for attachments --&gt;
&lt;!ELEMENT teaser (#PCDATA)&gt;&lt;!-- short hint on recent print and newest issue online --&gt;
&lt;!ELEMENT preface (#PCDATA)&gt;&lt;!-- link to preface, html byproduct of ePUB workflow --&gt;
&lt;!ELEMENT references (#PCDATA)&gt;&lt;!-- link to references, html byproduct of ePUB workflow --&gt;
&lt;!ELEMENT info (#PCDATA)&gt;&lt;!-- short information in between two issues, e.g. digital release, added as child to the last published issue, newest first --&gt;
&lt;!ATTLIST info date CDATA #IMPLIED&gt;&lt;!-- date for information, if missing the date of the parent schleuder will be used --&gt;

&lt;!ENTITY PUBLISHER "Chaos Computer Club"&gt;

]&gt;&#xa;
</xsl:text>
		<xsl:element name="schleudern"><xsl:text>&#xa;</xsl:text>

<!--
			<xsl:apply-templates select="//comment()[position()=2]"/>
-->

			<xsl:for-each select="schleuder">
			
				<xsl:element name="schleuder">
					<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
					<xsl:if test="@printonly != ''">
						<xsl:attribute name="printonly"><xsl:value-of select="@printonly"/></xsl:attribute>
					</xsl:if>
					<xsl:text>&#xa;</xsl:text>
					<xsl:if test="count(date) &gt; 0">
						<xsl:text>&#9;</xsl:text><xsl:element name="date"><xsl:value-of select="date"/></xsl:element><xsl:text>&#xa;</xsl:text>
					</xsl:if>
					<xsl:text>&#9;</xsl:text><xsl:element name="image"><xsl:value-of select="image"/></xsl:element><xsl:text>&#xa;</xsl:text>
					<xsl:for-each select="link">
						<xsl:text>&#9;</xsl:text><xsl:element name="link">
							<xsl:if test="count(@filesize) &gt; 0">
								<xsl:attribute name="filesize"><xsl:value-of select="@filesize"/></xsl:attribute>
							</xsl:if>
							<xsl:value-of select="."/>
						</xsl:element><xsl:text>&#xa;</xsl:text>
<!--						<xsl:if test="position() = 1">
						</xsl:if>-->
					</xsl:for-each>
					<xsl:if test="count(filesize) &gt; 0">
						<xsl:text>&#9;</xsl:text><xsl:element name="filesize"><xsl:value-of select="filesize"/></xsl:element><xsl:text>&#xa;</xsl:text>
					</xsl:if>
					<xsl:if test="count(teaser) &gt; 0">
						<xsl:text>&#9;</xsl:text>
						<xsl:apply-templates select="teaser"/>
						<xsl:text>&#xa;</xsl:text>
					</xsl:if>
					<xsl:if test="count(preface) &gt; 0">
						<xsl:text>&#9;</xsl:text><xsl:element name="preface">
								<xsl:value-of select="preface"/>
						</xsl:element><xsl:text>&#xa;</xsl:text>
					</xsl:if>
					<xsl:if test="count(references) &gt; 0">
						<xsl:text>&#9;</xsl:text><xsl:element name="references">
								<xsl:value-of select="references"/>
						</xsl:element><xsl:text>&#xa;</xsl:text>
					</xsl:if>
					<xsl:for-each select="info">
						<xsl:text>&#9;</xsl:text>
						<xsl:apply-templates select="."/>
						<xsl:text>&#xa;</xsl:text>
					</xsl:for-each>
				</xsl:element>
				<xsl:text>&#xa;</xsl:text>
			
			</xsl:for-each>

		</xsl:element>
	</xsl:template>
	<xsl:template match="comment()"><xsl:comment><xsl:value-of select="."/></xsl:comment></xsl:template>
	<xsl:template match="@*|node()"><!-- matches all -->
		<xsl:copy><!-- copy subtree -->
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
