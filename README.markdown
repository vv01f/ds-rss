# ds-rss

Generate parts of the datenschleuder website from source `data.xml`:

* rss feed as `ds-feed.xml`,
* oldfashioned hypertext `download.html` and
* the informative `index.html`

utilizing XML transformations.

And supporting to manage the `data.xml`.

## Cover

* extract: `pdftk pdfs/${if}.pdf cat 1 output covers/${of}.pdf`
* convert: `convert covers/${of}.pdf covers/${of}.jpg`

## using

Simply call `./gen.sh build` for generating the target files.

* Creating a new entry for `data.xml` is supported with `./gen.sh release` or `./gen.sh release 99` for a custom issue 
number, here 99.

* To omit the explaining information, simply disregard stderr: `./gen.sh release 2>/dev/null`.

* Replicating entry for ds #99 including the datetime: `./gen.sh release 99 1568505600`.

* the release message is whatever are the remaining non-numeric arguments: `./gen.sh release 99 1568505600 Die Ausgabe 
\#100 ist schon einige Zeit fertig und ausgeliefert. Die Freunde der Digitalen Ausgabe wird freuen, dass die Aushabe 
\#99 nun online ist.`

* For sorting `data.xml`, call `./gen.sh sortxml`. (writes `sorted.xml` and asks if it should replace the source)

## dependecies

* sh, coreutils, date, sed etc.
* xsltproc

## preview: screenshots

<img src="ss/ss-index.png"  width="220">

<img src="ss/ss-download.png"  width="220">

<img src="ss/ss-feed.png"  width="220">

## reverse timestamp for specific date time

`date --date="Wed, 10 Nov 2021 08:33 +0000" +"%s"`
