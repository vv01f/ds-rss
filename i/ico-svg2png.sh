#!/usr/bin/env sh
command -v inkscape || {
	echo "missing inkscape"
	exit 1
}
if=fileicons.svg
#~ iconsize=16 # in px
for iconsize in $(echo 64 ); do # in px, assuming its a square, a = iconsize; smaller than 64 is unreadable 
	for ofi in $(echo epub html pdf magnet other); do

		# output filename
		filetype="png"
		of="new-"${ofi}"-"${iconsize}"."${filetype}
		# needs group obkect to be renamed accordingly and layer visible
		# option `--verb=LayerShowAll` works with GUI only, possibility: split SVG in separate files beforhand
			#~ --export-id-only --export-id=${ofi}"-prod" 
		inkscape ${if} \
			--export-area-page \
			--export-type=${filetype} \
			--export-filename=${of} \
			--export-id-only \
			--export-id ${ofi}-"prod" \
			--export-background-opacity=0 \
			--export-width=${iconsize} \
			--export-height=${iconsize} \
			 --batch-process

	done
done
