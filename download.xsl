<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/schleudern">
	<xsl:variable name="protocol">https://</xsl:variable>
	<xsl:variable name="baseurl">ds.ccc.de</xsl:variable>
<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>Die Datenschleuder :: Download</title>
	<meta name="robots" content="index,follow" />
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/" />
	<link rel="alternate" type="application/rss+xml" title="Datenschleuder als RSS-Feed abonnieren" href="/ds-feed.xml" />
	<meta name="dc.publisher" content="Chaos Computer Club" />
	<link rel="bookmark" href="http://www.ccc.de/" title="Chaos Computer Club" type="text/html" />
	<link rel="bookmark" href="http://chaosradio.ccc.de/" title="Chaosradio" type="text/html" />
	<link rel="bookmark" href="http://events.ccc.de/" title="CCC Events Weblog" type="text/html" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="/ie/ie7-server.css"><![endif]-->
	<style type="text/css" media="screen,projection"> @import "style.css"; .icons { width: 16px; } .schleuder { overflow: hidden; } </style>
</head><body>
<xsl:element name="div">
<a name="top"/><img src="i/ds_headline.gif" alt="Die Datenschleuder"/><br/>
<img src="i/ds_subline.gif" alt="Das wissenschaftliche Fachblatt für Datenreisende - Ein Organ des Chaos Computer Club" /></xsl:element>

<p><a href="index.html">Die Datenschleuder</a> | <a href="contribute.html">Contribute</a> | Download | <a href="/contact.html">Kontakt</a> | <a href="/imprint.html">Impressum/Datenschutz</a> | <a href="/order.html">Bestellen</a></p>
<div style="background: #dddddd; padding: 1px 15px 10px 15px">
	<h3>Ältere Ausgaben</h3>
	<p xmlnsi="" lang="de">
		Hier stellen wir ältere Ausgaben unseres Mitgliedermagazins digital zur Verfügung.
		Die jeweils aktuelle Ausgabe wird Mitgliedern zugesendet.
		Die die digitale Veröffentlichung erfolgt ohne besondere Priorität zeitversetzt.
	</p>

	<xsl:element name="div">
		<xsl:attribute name="id">schleudern</xsl:attribute>
		<xsl:attribute name="class">schleudern</xsl:attribute>
		<xsl:for-each select="schleuder">
<!--
		<xsl:if test="not(@printonly)">
-->
			<xsl:variable name="id">
				<xsl:value-of select="@id"/>
			</xsl:variable>
			<xsl:variable name="alt"><xsl:value-of select="concat('ds',$id)"/></xsl:variable>
			<xsl:variable name="link">
				<xsl:value-of select="link"/>
			</xsl:variable>
			<xsl:variable name="type">
				<xsl:choose>
					<xsl:when test="substring($link, string-length($link) - 3) = 'html'">html</xsl:when>
					<xsl:when test="substring($link, string-length($link) - 2) = 'pdf'">pdf</xsl:when>
					<xsl:when test="substring($link, string-length($link) - 2) = 'zip'">other</xsl:when>
					<xsl:when test="substring($link, string-length($link) - 6) = 'torrent'">other</xsl:when>
					<xsl:otherwise>unknown</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="preface">
				<xsl:value-of select="preface"/>
			</xsl:variable>
			<xsl:variable name="references">
				<xsl:value-of select="references"/>
			</xsl:variable>
			<xsl:element name="div">
				<xsl:attribute name="class">schleuder</xsl:attribute>
				<xsl:attribute name="id">ds<xsl:value-of select="$id"/></xsl:attribute>
				<xsl:element name="div">
					<xsl:attribute name="class">schleudercover</xsl:attribute>
					<xsl:choose>
						<xsl:when test="link != ''">
							<xsl:element name="a">
								<xsl:attribute name="href">
									<xsl:choose>
										<xsl:when test="contains(link,'://')">
											<xsl:value-of select="link"/>
										</xsl:when>
										<!--xsl:when test="contains(link,'mailto:')">
											<xsl:value-of select="link"/>
										</xsl:when-->
										<xsl:otherwise>
											<xsl:value-of select="concat($protocol,$baseurl,'/',link)"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<xsl:element name="img">
									<xsl:attribute name="src">
										<xsl:choose>
											<xsl:when test="image != ''"><xsl:value-of select="image"/></xsl:when>
											<xsl:otherwise>covers/dummy.png</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:attribute name="alt"><xsl:value-of select="$alt"/></xsl:attribute>
									<xsl:attribute name="width">105</xsl:attribute>
									<xsl:attribute name="height">150</xsl:attribute>
								</xsl:element>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="img">
								<xsl:attribute name="src"><xsl:value-of select="image"/></xsl:attribute>
								<xsl:attribute name="alt"><xsl:value-of select="$alt"/></xsl:attribute>
								<xsl:attribute name="width">105</xsl:attribute>
								<xsl:attribute name="height">150</xsl:attribute>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element><!--
				--><xsl:choose>
					<xsl:when test="count(references) &gt; 0">
						<xsl:element name="a">
							<xsl:attribute name="href"><xsl:value-of select="concat($protocol,$baseurl,'/',references)"/></xsl:attribute>
							<xsl:attribute name="title"><xsl:value-of select="concat('Rerenzen zur ',$alt)"/></xsl:attribute>
							<xsl:value-of select="concat(@id,'')"/>
						</xsl:element><xsl:text>&#32;&#10;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat(@id,' ')"/>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:for-each select="link">
					<xsl:call-template name="anchorlink">
						<xsl:with-param name="linkdata">
							<xsl:value-of select="." />
						</xsl:with-param>
						<xsl:with-param name="baseuri">
							<xsl:choose>
								<xsl:when test="contains(., '://')"></xsl:when>
								<!--xsl:when test="contains(., '')"></xsl:when--><!-- other conditions could be applicable without the "//" in uri, e.g. "mailto:john@doe.com" -->
								<!--xsl:when test="contains(., 'mailto:')">
									<xsl:value-of select=""/>
								</xsl:when-->
								<xsl:otherwise>
									<xsl:value-of select="concat($protocol,$baseurl,'/')" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:if test="not(link)"><!-- fix for no icon glitch -->
					<xsl:element name="span">
						<xsl:attribute name="style">display: inline-block; height: 16px; width: 1px; margin-top: 3px;</xsl:attribute>
					</xsl:element>
				</xsl:if>
			</xsl:element>
<!--
		</xsl:if>
-->
	</xsl:for-each>
	<xsl:element name="div">
		<xsl:attribute name="style">clear: left</xsl:attribute>
	</xsl:element>
</xsl:element>
</div>
</body>
</html>
	</xsl:template>
	<xsl:template name="anchorlink">
		<xsl:param name="linkdata" />
		<xsl:param name="typedata" />
		<xsl:param name="baseuri" /><!-- todo: replace baseuri with "./" if it is $baseurl, take care of linkdata starting with "/" or "." -->
		<xsl:choose>
			<xsl:when test="not($typedata)">
				<xsl:variable name="t">
					<xsl:choose>
						<xsl:when test="substring($linkdata, string-length($linkdata) - 2) = 'pdf'">pdf</xsl:when>
						<xsl:when test="substring($linkdata, string-length($linkdata) - 3) = 'epub'">epub</xsl:when>
						<xsl:when test="substring($linkdata, string-length($linkdata) - 3) = 'html'">html</xsl:when>
						<xsl:when test="substring($linkdata, string-length($linkdata) - 2) = 'zip'">other</xsl:when>
						<xsl:when test="substring($linkdata, string-length($linkdata) - 6) = 'torrent'">other</xsl:when>
						<xsl:otherwise>unknown</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:call-template name="anchorlink">
					<xsl:with-param name="linkdata">
						<xsl:choose>
							<xsl:when test="not($baseuri)">
								<xsl:value-of select="$linkdata"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat($baseuri,$linkdata)"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="typedata" select="$t" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="a">
					<xsl:attribute name="href"><xsl:value-of select="$linkdata"/></xsl:attribute>
					<xsl:attribute name="title"><xsl:value-of select="concat($typedata,'-Datei')"/></xsl:attribute>
					<xsl:if test="($typedata != 'unknown')">
						<xsl:element name="img">
							<xsl:attribute name="src"><xsl:value-of select="concat('i/',$typedata,'.png')"/>
							</xsl:attribute>
							<xsl:attribute name="alt"><xsl:value-of select="$typedata"/></xsl:attribute>
							<xsl:attribute name="class">icons</xsl:attribute>
						</xsl:element>
					</xsl:if>
				</xsl:element><xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="string-to-lower">
		<xsl:param name="string"/>
		<xsl:param name="forspace"> </xsl:param>
		<xsl:value-of select="translate($string, ' ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ', concat($forspace,'abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ'))"/>
	</xsl:template>

	<xsl:template name="string-to-upper">
		<xsl:param name="string"/>
		<xsl:param name="forspace"> </xsl:param>
		<xsl:value-of select="translate($string,' abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ',concat($forspace, 'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ'))"/>
	</xsl:template>
<!--
	<xsl:call-template name="anchorlink"/>
		<xsl:with-param name="linkdata" select="link" />
	</xsl:call-template>
-->
</xsl:stylesheet>
