#!/bin/sh
fn="../references.html"
cat > index.html <<EOF
<!DOCTYPE html><html xml:lang="de" lang="de"><head>
<meta charset="utf-8"/>
<meta http-equiv="refresh" content="3; URL=https://ds.ccc.de/references.html">
</head><body>
Der Inhalt, den sie sichen liegt vermutlich unter <a href="https://ds.ccc.de/references.html">https://ds.ccc.de/references.html</a>
</body></html>
EOF
cat > ${fn} <<EOF
<!DOCTYPE html><html xml:lang="de" lang="de"><head>
<title>Referenzen zu einzelnen Ausgaben der Datenschleuder</title>
<meta charset="utf-8"/><meta name="robots" content="index,follow"/><link rel="schema.DC" href="http://purl.org/dc/terms/"/><link rel="alternate" type="application/rss+xml" title="Datenschleuder als RSS-Feed abonnieren" href="ds-feed.xml"/><meta name="dc.publisher" content="Chaos Computer Club"/><meta name="viewport" content="width=device-width, initial-scale=1"/><style type="text/css" media="screen,projection"> @import "style.css"; 
time { font-family: Arial, sans-serif; font-size: 10pt; }</style>
</head><body>
<p id="top"><img src="i/ds_headline.gif" alt="Die Datenschleuder"/><br/><img src="i/ds_subline.gif" alt="Das wissenschaftliche Fachblatt f&#xFC;r Datenreisende &#x2013; Ein Organ des Chaos Computer Club"/></p>
<p><a href="index.html">Die Datenschleuder</a> | Referenzen | <a href="contribute.html">Contribute</a> | <a href="download.html">Download</a> | <a href="contact.html">Kontakt</a> | <a href="imprint.html">Impressum/Datenschutz</a> | <a href="order.html">Bestellen</a></p>
<div style="background: #dddddd; padding: 1px 15px 10px 15px;">

<h2>Referenzen zu einzelnen Ausgaben der Datenschleuder</h2>
<!-- Vorbehaltlich eines Redesigns für die Downloads hier erstmal eine Auflistung der Ausgaben mit Referenzen online. Im Idealfall wird diese ebenso wie Geleitworte in die Downloads integriert. -->
<p>
<ul>
EOF
for n in $( ls ds*.html|cut -c3- |cut -d'.' -f1|sort -n|tac ); do 
  echo "<li><a href=\"references/ds${n}.html\">Ausgabe $n</a></li>" >> ${fn}
done
cat >> ${fn} <<EOF
</ul>
</p>

</div></body></html>
EOF
